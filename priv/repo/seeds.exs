# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     CheetahApi.Repo.insert!(%CheetahApi.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias CheetahApi.Repo
alias CheetahApi.Account.Role
alias CheetahApi.Account.User
alias CheetahApi.Discussion.JobType
alias CheetahApi.Discussion.Job
# require IEx

insert_timestamp_value = fn(data) ->
  data |> Enum.map(fn(row) -> row |> 
            Map.put(:inserted_at, DateTime.utc_now) |>
            Map.put(:updated_at,  DateTime.utc_now)
  end)
end

# IEx.pry

roles =  [%{name: "super_admin"},
          %{name: "site_admin"},
          %{name: "student"},
          %{name: "faculty"},
          %{name: "alumni"}] |> insert_timestamp_value.()

Repo.insert_all(Role, [])

# ============================Creating User =================================

user_params = %{
        email_address: "test@aviabird.com",
        gender: "male", 
        dob: "2014-04-17T14:00:00Z",
        first_name: "test", 
        last_name: "aviabird",
        password: "test12345", 
        mobile_number: "1234567890", 
        current_city: "test_city"
      }


user = User.registration_changeset(%User{}, user_params)

Repo.insert(user)

#================== Create Job Types =========================
job_types = [%{type: "fulltime"}, 
             %{type: "internship"}, 
             %{type: "parttime"}] |> insert_timestamp_value.()


Repo.insert_all(JobType, job_types)
#==================================================================

#=========================Create Job ==============================
job_params = Job.changeset(%Job{}, 
              %{
                title: "test_title",
                company: "Aviabird",
                location: "test_location",
                contact_email: "test@test_domain.com",
                description: "test description",
                user_id: 1,
                job_type_id: :random.uniform(3)
              })

Repo.insert(job_params)

#===================================================================



