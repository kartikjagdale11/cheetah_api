defmodule CheetahApi.Repo.Migrations.WorkExperienceSkills do
  use Ecto.Migration

  def change do
    create table(:work_experience_skills) do
      add :work_experience_id, references(:work_experiences, on_delete: :delete_all)
      add :skill_id, references(:skills)
    end

    create index(:work_experience_skills, [:work_experience_id])
    create index(:work_experience_skills, [:skill_id])
    create unique_index(:work_experience_skills, [:work_experience_id, :skill_id])
  end
end
