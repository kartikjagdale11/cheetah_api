defmodule CheetahApi.Repo.Migrations.CreateJobTypes do
  use Ecto.Migration

  def change do
    create table(:job_types) do
      add :type, :string
      timestamps()
    end
    create unique_index(:job_types, [:type])
  end
end
