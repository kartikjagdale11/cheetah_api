defmodule CheetahApi.Repo.Migrations.WorkDetails do
  use Ecto.Migration

  def change do
    create table(:work_details) do
      add :description, :text
      add :start_date, :date
      add :end_date, :date
      add :location, :string
      
      add :work_experience_id, references(:work_experiences, on_delete: :delete_all)
      add :professional_role_id, references(:professional_roles)
      add :company_id, references(:companies)

      timestamps()
    end

      create index(:work_details, [:work_experience_id])
  end
end
