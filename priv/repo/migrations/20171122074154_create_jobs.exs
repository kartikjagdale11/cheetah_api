defmodule CheetahApi.Repo.Migrations.CreateJobs do
  use Ecto.Migration

  def change do
    create table(:jobs) do
      add :user_id, references(:users, on_delete: :delete_all)
      add :job_type_id, references(:job_types)
      add :title, :string
      add :company, :string
      add :location, :string
      add :contact_email, :string
      add :description, :text
      timestamps()
    end
  end
end
