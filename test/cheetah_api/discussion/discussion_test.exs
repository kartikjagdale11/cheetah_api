defmodule CheetahApi.DiscussionTest do
  use CheetahApi.DataCase

  alias CheetahApi.Discussion

  describe "job_types" do
    alias CheetahApi.Discussion.JobType

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def job_type_fixture(attrs \\ %{}) do
      {:ok, job_type} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Discussion.create_job_type()

      job_type
    end

    test "list_job_types/0 returns all job_types" do
      job_type = job_type_fixture()
      assert Discussion.list_job_types() == [job_type]
    end

    test "get_job_type!/1 returns the job_type with given id" do
      job_type = job_type_fixture()
      assert Discussion.get_job_type!(job_type.id) == job_type
    end

    test "create_job_type/1 with valid data creates a job_type" do
      assert {:ok, %JobType{} = job_type} = Discussion.create_job_type(@valid_attrs)
    end

    test "create_job_type/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Discussion.create_job_type(@invalid_attrs)
    end

    test "update_job_type/2 with valid data updates the job_type" do
      job_type = job_type_fixture()
      assert {:ok, job_type} = Discussion.update_job_type(job_type, @update_attrs)
      assert %JobType{} = job_type
    end

    test "update_job_type/2 with invalid data returns error changeset" do
      job_type = job_type_fixture()
      assert {:error, %Ecto.Changeset{}} = Discussion.update_job_type(job_type, @invalid_attrs)
      assert job_type == Discussion.get_job_type!(job_type.id)
    end

    test "delete_job_type/1 deletes the job_type" do
      job_type = job_type_fixture()
      assert {:ok, %JobType{}} = Discussion.delete_job_type(job_type)
      assert_raise Ecto.NoResultsError, fn -> Discussion.get_job_type!(job_type.id) end
    end

    test "change_job_type/1 returns a job_type changeset" do
      job_type = job_type_fixture()
      assert %Ecto.Changeset{} = Discussion.change_job_type(job_type)
    end
  end

  describe "jobs" do
    alias CheetahApi.Discussion.Job

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def job_fixture(attrs \\ %{}) do
      {:ok, job} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Discussion.create_job()

      job
    end

    test "list_jobs/0 returns all jobs" do
      job = job_fixture()
      assert Discussion.list_jobs() == [job]
    end

    test "get_job!/1 returns the job with given id" do
      job = job_fixture()
      assert Discussion.get_job!(job.id) == job
    end

    test "create_job/1 with valid data creates a job" do
      assert {:ok, %Job{} = job} = Discussion.create_job(@valid_attrs)
    end

    test "create_job/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Discussion.create_job(@invalid_attrs)
    end

    test "update_job/2 with valid data updates the job" do
      job = job_fixture()
      assert {:ok, job} = Discussion.update_job(job, @update_attrs)
      assert %Job{} = job
    end

    test "update_job/2 with invalid data returns error changeset" do
      job = job_fixture()
      assert {:error, %Ecto.Changeset{}} = Discussion.update_job(job, @invalid_attrs)
      assert job == Discussion.get_job!(job.id)
    end

    test "delete_job/1 deletes the job" do
      job = job_fixture()
      assert {:ok, %Job{}} = Discussion.delete_job(job)
      assert_raise Ecto.NoResultsError, fn -> Discussion.get_job!(job.id) end
    end

    test "change_job/1 returns a job changeset" do
      job = job_fixture()
      assert %Ecto.Changeset{} = Discussion.change_job(job)
    end
  end
end
