ExUnit.start

defmodule CheetahApi.AccountTest do
  use CheetahApiWeb.ConnCase

  alias CheetahApi.Account

  @test_email "test@gmail.com"
  @invalid_email "test"

  test "create account for valid user" do
    {:ok, user } = Account.create(%{"email_address" => @test_email})

    assert user.email_address == @test_email
  end

  test "create account with duplicate email" do
    {:ok, _ } = Account.create(%{"email_address" => @test_email})
    {:error, changeset } = Account.create(%{"email_address" => @test_email})
    {error_msg, []} = Keyword.get(changeset.errors, :email_address)

    assert error_msg == "has already been taken"
  end

  test "create account with invalid email" do
    {:error, changeset } = Account.create(%{"email_address" => @invalid_email})
    {error_msg, _} = Keyword.get(changeset.errors, :email_address)

    assert error_msg == "has invalid format"
  end

end
