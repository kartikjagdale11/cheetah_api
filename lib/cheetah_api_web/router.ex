defmodule CheetahApiWeb.Router do
  use CheetahApiWeb, :router

  pipeline :api do
    plug :accepts, ["json-api", "json"]
    plug JaSerializer.Deserializer
  end

  scope "/api", CheetahApiWeb do
    pipe_through :api

    post "/register_email", AccountController, :register_email
    post "/create_account", AccountController, :create_account
    get "/profile", UserController, :show
    resources "/job", JobController
  end
end
