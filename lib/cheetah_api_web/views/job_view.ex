defmodule CheetahApiWeb.JobView do
  use CheetahApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [
    :user_id,
    :job_type_id,
    :title,
    :company,
    :location,
    :contact_email,
    :description,
    :job_type
  ]

  def job_type(job, _conn) do
    job.job_type.type
  end

end
