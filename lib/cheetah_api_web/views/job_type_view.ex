defmodule CheetahApiWeb.JobTypeView do
  use CheetahApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [:type]
end
