defmodule CheetahApiWeb.JobController do
  use CheetahApiWeb, :controller
  alias CheetahApi.Discussion
  alias CheetahApi.Discussion.Job
  
  action_fallback CheetahApiWeb.FallbackController
  
  # TODO: Add pagination, for this send 
  # limits in params.
  # @spec index(Conn.t, _params) :: Conn.t
  def index(%Conn{} = conn, _params) do
    with jobs <- Discussion.list_jobs do
      conn |> render("index.json-api", data: jobs)
    end
  end

  def show do
    
  end

  def new do
    
  end

  def edit do
    
  end

  def create do
    
  end

  def update do
    
  end

  def delete do
    
  end

end