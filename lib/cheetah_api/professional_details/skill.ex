defmodule CheetahApi.ProfessionalDetails.Skill do
  use CheetahApi.Data

  schema "skills" do
    field :name, :string

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
      |> cast(params, [:name])
      |> validate_required([:name])
  end

end