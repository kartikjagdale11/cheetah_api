defmodule CheetahApi.ProfessionalDetails.WorkExperience do
  use CheetahApi.Data
  
  alias CheetahApi.ProfessionalDetails.{ProfessionalRole, Industry, Skill, WorkDetail}
  alias CheetahApi.UserProfile.User

  schema "work_experiences" do
    field :professional_headline, :string
    field :total_years_experience, :integer

    has_many :work_details, WorkDetail

    many_to_many :professional_roles, ProfessionalRole, join_through: "work_experience_professional_roles"
    many_to_many :industries, Industry, join_through: "work_experience_industries"
    many_to_many :skills, Skill, join_through: "work_experience_skills"

    belongs_to :user, User

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
      |> cast(params, [:professional_headline, :total_years_experience, :user_id])
      |> validate_required([:total_years_experience, :user_id])
      |> put_assoc(:professional_roles, load_assoc(params[:professional_role_ids], ProfessionalRole))
      |> put_assoc(:industries, load_assoc(params[:industry_ids], Industry))
      |> put_assoc(:skills, load_assoc(params[:skill_ids], Skill))
      |> cast_assoc(:work_details)
  end

  defp load_assoc(ids, schema) do
    Repo.all(from s in schema, where: s.id in ^ids)
  end

end