defmodule CheetahApi.ProfessionalDetails do
  use CheetahApi.Data

  alias CheetahApi.ProfessionalDetails.{WorkExperience, WorkDetail}

  @doc """
  Creates a Work Experience.
  ## Examples
      params = %{total_years_experience: 5, user_id: 1, professional_role_ids: [1,2], industry_ids: [1,2], skill_ids: [1,2], work_details: %{0 => %{professional_role_id: 1, company_id: 1}}}
      iex> create_work_experience(params)
      {:ok, %WorkExperience{}}
  """
  def create_work_experience(params) do
    changeset = WorkExperience.changeset(%WorkExperience{}, params)
    IO.inspect changeset
    Repo.insert(changeset)
  end


  @doc """
  Creates a Work Detail.
  ## Examples
      params = %{professional_role_id: 1, company_id: 1, work_experience_id: 1}
      iex> create_work_detail(params)
      {:ok, %WorkDetail{}}
  """
  def create_work_detail(params) do
    changeset = WorkDetail.changeset(%WorkDetail{}, params)
    IO.inspect changeset
    Repo.insert(changeset)
  end

end