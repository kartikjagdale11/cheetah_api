defmodule CheetahApi.ProfessionalDetails.WorkDetail do
  use CheetahApi.Data

  alias CheetahApi.ProfessionalDetails.{ProfessionalRole, Company, WorkExperience}

  schema "work_details" do
    field :description, :string
    field :start_date, :date
    field :end_date, :date
    field :location, :string

    belongs_to :work_experience, WorkExperience
    belongs_to :professional_role, ProfessionalRole
    belongs_to :company, Company

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
      |> cast(params, [:description, :start_date, :work_experience_id, :professional_role_id, :company_id])
      |> validate_required([:start_date, :professional_role_id, :company_id])
  end

end
