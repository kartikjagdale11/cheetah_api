defmodule CheetahApi.Discussion.Job do
  use CheetahApi.Data
  alias CheetahApi.Discussion.{ Job, JobType }
  alias CheetahApi.Account.User
  
  @job_fields [:title, :company, :location, 
               :contact_email, :description, 
               :job_type_id, :user_id ]

  schema "jobs" do
    field :title, :string
    field :company, :string
    field :location, :string
    field :contact_email, :string
    field :description, :string
    belongs_to :user, User
    belongs_to :job_type, JobType
    timestamps()
  end

  @doc false
  def changeset(%Job{} = job, attrs) do
    job
    |> cast(attrs, @job_fields)
    |> validate_required([:title, :job_type_id, :user_id])
    |> validate_format(:contact_email, ~r/@/)
  end
end
