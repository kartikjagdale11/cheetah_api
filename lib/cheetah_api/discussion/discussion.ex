defmodule CheetahApi.Discussion do
  @moduledoc """
  The Discussion context.
  """

  import Ecto.Query, warn: false
  alias CheetahApi.Repo

  alias CheetahApi.Discussion.JobType
  @job_preloads [:job_type]
  @doc """
  Returns the list of job_types.

  ## Examples

      iex> list_job_types()
      [%JobType{}, ...]

  """
  def list_job_types do
    Repo.all(JobType)
  end

  @doc """
  Gets a single job_type.

  Raises `Ecto.NoResultsError` if the Job type does not exist.

  ## Examples

      iex> get_job_type!(123)
      %JobType{}

      iex> get_job_type!(456)
      ** (Ecto.NoResultsError)

  """
  def get_job_type!(id), do: Repo.get!(JobType, id)

  @doc """
  Creates a job_type.

  ## Examples

      iex> create_job_type(%{field: value})
      {:ok, %JobType{}}

      iex> create_job_type(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_job_type(attrs \\ %{}) do
    %JobType{}
    |> JobType.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a job_type.

  ## Examples

      iex> update_job_type(job_type, %{field: new_value})
      {:ok, %JobType{}}

      iex> update_job_type(job_type, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_job_type(%JobType{} = job_type, attrs) do
    job_type
    |> JobType.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a JobType.

  ## Examples

      iex> delete_job_type(job_type)
      {:ok, %JobType{}}

      iex> delete_job_type(job_type)
      {:error, %Ecto.Changeset{}}

  """
  def delete_job_type(%JobType{} = job_type) do
    Repo.delete(job_type)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking job_type changes.

  ## Examples

      iex> change_job_type(job_type)
      %Ecto.Changeset{source: %JobType{}}

  """
  def change_job_type(%JobType{} = job_type) do
    JobType.changeset(job_type, %{})
  end

  alias CheetahApi.Discussion.Job

  @doc """
  Returns the list of jobs.

  ## Examples

      iex> list_jobs()
      [%Job{}, ...]

  """
  def list_jobs do
    Repo.all(Job) |> Repo.preload(@job_preloads)
  end

  @doc """
  Gets a single job.

  Raises `Ecto.NoResultsError` if the Job does not exist.

  ## Examples

      iex> get_job!(123)
      %Job{}

      iex> get_job!(456)
      ** (Ecto.NoResultsError)

  """
  def get_job!(id), do: Repo.get!(Job, id)

  @doc """
  Creates a job.

  ## Examples

      iex> create_job(%{field: value})
      {:ok, %Job{}}

      iex> create_job(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_job(attrs \\ %{}) do
    %Job{}
    |> Job.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a job.

  ## Examples

      iex> update_job(job, %{field: new_value})
      {:ok, %Job{}}

      iex> update_job(job, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_job(%Job{} = job, attrs) do
    job
    |> Job.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Job.

  ## Examples

      iex> delete_job(job)
      {:ok, %Job{}}

      iex> delete_job(job)
      {:error, %Ecto.Changeset{}}

  """
  def delete_job(%Job{} = job) do
    Repo.delete(job)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking job changes.

  ## Examples

      iex> change_job(job)
      %Ecto.Changeset{source: %Job{}}

  """
  def change_job(%Job{} = job) do
    Job.changeset(job, %{})
  end
end
