defmodule CheetahApi.Discussion.JobType do
  use CheetahApi.Data
  alias CheetahApi.Discussion.JobType


  schema "job_types" do
    field :type, :string
    has_many :jobs, Job
    timestamps()
  end

  @doc false
  def changeset(%JobType{} = job_type, attrs) do
    job_type
    |> cast(attrs, [:type])
    |> validate_required([:type])
  end
end
