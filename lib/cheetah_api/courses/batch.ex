defmodule CheetahApi.Courses.Batch do
  use Ecto.Schema
  import Ecto.Changeset
  alias CheetahApi.Courses.{Batch, Stream}


  schema "batches" do
    field :end_date, :date
    field :start_date, :date

    belongs_to :stream, Stream

    timestamps()
  end

  @doc false
  def changeset(%Batch{} = batch, attrs) do
    batch
    |> cast(attrs, [:start_date, :end_date])
    |> validate_required([:start_date, :end_date])
  end
end
