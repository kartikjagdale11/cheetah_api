defmodule CheetahApi.UserProfile do
  use CheetahApi.Data

  alias CheetahApi.UserProfile.User

  def find(%{"id" => id}) do
    User |> Repo.get(id)
  end

end