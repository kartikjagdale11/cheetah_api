defmodule CheetahApi.UserProfile.ContactDetail do
  use CheetahApi.Data

  alias CheetahApi.UserProfile.{User, CorrespondenceAddress}

  schema "contact_details" do
    field :primary_email, :string
    field :secondary_email, :string
    field :website_url, :string

    has_one :correspondence_address, CorrespondenceAddress

    belongs_to :user, User

    timestamps()
  end

end