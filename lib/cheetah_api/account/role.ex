defmodule CheetahApi.Account.Role do
  use CheetahApi.Data
  alias CheetahApi.Account.Role

  schema "roles" do
    field :name, :string
    timestamps()
  end

  @doc false
  def changeset(%Role{} = role, attrs) do
    role
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
