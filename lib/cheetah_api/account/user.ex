defmodule CheetahApi.Account.User do
  use CheetahApi.Data
  alias CheetahApi.Account.Role

  alias Comeonin.Bcrypt
  @reg_fields  [:email_address, :first_name, :last_name, :gender,
            :dob, :mobile_number, :current_city, :password];
  schema "users" do
    field :email_address, :string
    field :gender, :string
    field :dob, :date
    field :mobile_number, :string
    field :current_city, :string
    field :hashed_password, :string
    field :first_name, :string
    field :last_name, :string

    field :password, :string, virtual: true

    many_to_many :roles, Role, join_through: "user_roles"

    timestamps()
  end

  def email_changeset(struct, params\\ %{}) do
    struct
    |> cast(params, [:email_address])
    |> validate_required([:email_address])
    |> validate_format(:email_address, ~r/@/)
    |> unique_constraint(:email_address)
  end

  def registration_changeset(struct, params\\ %{}) do                
    struct
    |> cast(params, @reg_fields)
    |> validate_required(@reg_fields)
    |> validate_length(:password, min: 6, max: 15)
    |> hash_password()
  end

  def hash_password(%{valid?: false} = changeset), do: changeset
  def hash_password(%{valid?: true} = changeset) do
    hashed_password =
      changeset
      |> get_field(:password)
      |> Bcrypt.hashpwsalt()
    put_change(changeset, :hashed_password, hashed_password)
  end
end
